require('./css/main.css');
require('./js/index.js');
document.addEventListener("DOMContentLoaded", function () {
    const divElement = document.querySelector("div");
  
    // Function to change the background color to red
    function changeToRed() {
        divElement.style.backgroundColor = "red";
    }
  
    // Function to change the background color to lavender
    function changeToLavender() {
        divElement.style.backgroundColor = "lavender";
    }
  
    // Function to change the background color to grey
    function changeToGrey() {
        divElement.style.backgroundColor = "grey";
    }
  
    // Get the buttons by their IDs
    const redButton = document.getElementById("redButton");
    const lavenderButton = document.getElementById("lavenderButton");
    const greyButton = document.getElementById("greyButton");
  
    // Add click event listeners to the buttons
    redButton.addEventListener("click", changeToRed);
    lavenderButton.addEventListener("click", changeToLavender);
    greyButton.addEventListener("click", changeToGrey);
  });
  